package com.aalmeida.logging;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by aalmeida on 08/03/2017.
 */
final class HttpRequest {

    static void sendPostRequest(final String requestUrl, final byte[] out) throws IOException {
        for (int i = 0; i < 3; i++) {
            final URL url = new URL(requestUrl);
            final HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
            try {
                httpCon.setDoOutput(true);
                httpCon.setRequestMethod("POST");
                httpCon.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                httpCon.setFixedLengthStreamingMode(out.length);
                httpCon.connect();
                try (OutputStream os = httpCon.getOutputStream()) {
                    os.write(out);
                    if (httpCon.getResponseCode() == 200) {
                        break;
                    }
                }
            } finally {
                httpCon.disconnect();
            }
        }
    }
}
