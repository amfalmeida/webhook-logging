package com.aalmeida.logging;

import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.appender.AppenderLoggingException;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;

import java.io.Serializable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by aalmeida on 08/03/2017.
 */
@Plugin(name="Webhook", category="Core", elementType="appender", printObject=true)
public class Webhook extends AbstractAppender {

    private final ReadWriteLock rwLock = new ReentrantReadWriteLock();
    private final Lock readLock = rwLock.readLock();

    private final String requestUrl;

    private Webhook(String name, Filter filter, Layout<? extends Serializable> layout, final String pRequestUrl) {
        super(name, filter, layout, false);
        requestUrl = pRequestUrl;
    }

    @Override
    public void append(LogEvent event) {
        readLock.lock();
        try {
            final byte[] bytes = getLayout().toByteArray(event);
            HttpRequest.sendPostRequest(requestUrl, bytes);
            //System.out.write(bytes);
        } catch (Exception ex) {
            if (!ignoreExceptions()) {
                throw new AppenderLoggingException(ex);
            }
        } finally {
            readLock.unlock();
        }
    }

    @PluginFactory
    public static Webhook createAppender(
            @PluginAttribute("name") String name,
            @PluginAttribute("requestUrl") String requestUrl,
            @PluginElement("Layout") Layout<? extends Serializable> layout,
            @PluginElement("Filter") final Filter filter) {
        if (name == null || name.trim().isEmpty()) {
            LOGGER.error("No name provided for Webhook.");
            return null;
        }
        if (requestUrl == null || requestUrl.trim().isEmpty()) {
            LOGGER.error("No request url provided for Webhook.");
            return null;
        }
        if (layout == null) {
            layout = PatternLayout.createDefaultLayout();
        }
        return new Webhook(name, filter, layout, requestUrl);
    }
}