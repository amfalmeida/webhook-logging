package com.aalmeida.logging;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by aalmeida on 08/03/2017.
 */
public class WebhookTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebhookTest.class);

    @Test
    public void logException() {
        try {
            throw new Exception("Webhook exception.");
        } catch (Exception e) {
            LOGGER.error("Webhook exception.", e);
        }
    }

    @Test
    public void logUsual() {
        LOGGER.debug("Webhook debug.");
        LOGGER.info("Webhook info.");
        LOGGER.warn("Webhook warn.");
    }
}
