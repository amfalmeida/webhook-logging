#Webhook Logging

Example of POST on a Slack channel:

```xml
<?xml version="1.0" encoding="UTF-8"?> 
<Configuration status="INFO" packages="com.aalmeida.logging">     
    <Appenders>
        <Console name="console" target="SYSTEM_OUT">
            <PatternLayout pattern="%d{HH:mm:ss.SSS} [%t] %-5level %logger{36} - %msg%n" />
        </Console>
        
        <Webhook name="file"
                 requestUrl="https://hooks.slack.com/services/T40KJ31E2/B4EV7HP7A/yyyyy">
            <PatternLayout pattern="payload={'text': '%-5level: %msg'}%n"/>
        </Webhook>     
    </Appenders>     
    
    <Loggers>         
        <Root level="DEBUG">             
            <AppenderRef ref="file"/>         
        </Root>         
        <Logger name="com.aalmeida" level="ERROR" additivity="false">             
            <AppenderRef ref="file" level="ERROR" />         
        </Logger>     
    </Loggers> 
</Configuration>
```


